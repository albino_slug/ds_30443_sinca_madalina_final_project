-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------
-- -----------------------------------------------------
-- Schema climbing_center
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema climbing_center
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `climbing_center` DEFAULT CHARACTER SET utf8mb4 ;
USE `climbing_center` ;

-- -----------------------------------------------------
-- Table `climbing_center`.`courses`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `climbing_center`.`courses` (
  `id` INT(11) NOT NULL,
  `description` VARCHAR(255) NOT NULL,
  `end_date` DATETIME NOT NULL,
  `name` VARCHAR(255) NOT NULL,
  `start_date` DATETIME NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `climbing_center`.`events`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `climbing_center`.`events` (
  `id` INT(11) NOT NULL,
  `date` DATETIME NOT NULL,
  `description` VARCHAR(255) NOT NULL,
  `name` VARCHAR(255) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `climbing_center`.`hibernate_sequence`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `climbing_center`.`hibernate_sequence` (
  `next_val` BIGINT(20) NULL DEFAULT NULL)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `climbing_center`.`users`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `climbing_center`.`users` (
  `id` INT(11) NOT NULL,
  `age` INT(11) NOT NULL,
  `email` VARCHAR(255) NOT NULL,
  `password` VARCHAR(255) NOT NULL,
  `role` VARCHAR(255) NOT NULL,
  `username` VARCHAR(255) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `climbing_center`.`user_course`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `climbing_center`.`user_course` (
  `user_id` INT(11) NOT NULL,
  `course_id` INT(11) NOT NULL,
  INDEX `FKf4qni5wnlq0x70fm39w9tv7x4` (`course_id` ASC),
  INDEX `FKoc4yl478i6o8hf240vumhjgrb` (`user_id` ASC),
  CONSTRAINT `FKf4qni5wnlq0x70fm39w9tv7x4`
    FOREIGN KEY (`course_id`)
    REFERENCES `climbing_center`.`courses` (`id`),
  CONSTRAINT `FKoc4yl478i6o8hf240vumhjgrb`
    FOREIGN KEY (`user_id`)
    REFERENCES `climbing_center`.`users` (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `climbing_center`.`user_event`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `climbing_center`.`user_event` (
  `user_id` INT(11) NOT NULL,
  `event_id` INT(11) NOT NULL,
  INDEX `FKj02k6c39rkjxpvslfqq3s6kmb` (`event_id` ASC),
  INDEX `FKk4yivyy0xvsm1l9kupdggqd2r` (`user_id` ASC),
  CONSTRAINT `FKj02k6c39rkjxpvslfqq3s6kmb`
    FOREIGN KEY (`event_id`)
    REFERENCES `climbing_center`.`events` (`id`),
  CONSTRAINT `FKk4yivyy0xvsm1l9kupdggqd2r`
    FOREIGN KEY (`user_id`)
    REFERENCES `climbing_center`.`users` (`id`))
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
