import { Component, OnInit } from '@angular/core';
import {first} from 'rxjs/operators';
import {ClimbingCourse, User} from '../_models';
import {CourseService, UserService} from '../_services';

@Component({
  selector: 'app-course-view',
  templateUrl: './course-view.component.html',
  styleUrls: ['./course-view.component.css']
})
export class CourseViewComponent implements OnInit {
  courses: ClimbingCourse[] = [];
  userCourses: ClimbingCourse[] = [];
  currentUser: User;

  constructor(
    private courseService: CourseService,
    private userService: UserService) {
    this.currentUser = JSON.parse(localStorage.getItem('currentUser'));

    this.loadAllCourses();
    this.loadAllUserCourses();
  }

  ngOnInit() {
  }

  private loadAllCourses() {
    this.courseService.findAll().pipe(first()).subscribe(courses => {
      this.courses = courses;
    });
  }

  private loadAllUserCourses() {
    this.userService.findCoursesByUserId(this.currentUser.id).pipe(first()).subscribe(
      courses => {
        this.userCourses = courses;
      });
  }

  enrollToCourse(id: number) {
    this.courseService.addUserById(this.currentUser.id, id).pipe(first()).subscribe(() => {
      this.loadAllUserCourses();
    });
  }

  disenrollFromCourse(id: number) {
    this.courseService.removeUserById(this.currentUser.id, id).pipe(first()).subscribe(() => {
      this.loadAllUserCourses();
    });
  }
}
