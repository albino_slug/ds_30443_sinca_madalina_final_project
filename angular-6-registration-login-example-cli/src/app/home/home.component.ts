﻿import { Component, OnInit } from '@angular/core';

import { User } from '../_models';
import {UserService} from '../_services';
import {first} from 'rxjs/operators';

@Component({templateUrl: 'home.component.html'})
export class HomeComponent implements OnInit {
    currentUser: User;
    imageURI: string;

    constructor(
      private userService: UserService
    ) {
        this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
    }

    ngOnInit() {
    }

  generateGraphics() {
      this.userService.generateGraphicStatistics().pipe(first()).subscribe(
        dataURI => {
        this.imageURI = dataURI.toString();
      });
  }
}
