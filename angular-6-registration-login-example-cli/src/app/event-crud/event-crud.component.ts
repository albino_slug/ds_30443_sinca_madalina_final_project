import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ClimbingEvent} from '../_models/climbing-event';
import {AlertService} from '../_services';
import {Router} from '@angular/router';
import {first} from 'rxjs/operators';
import {EventService} from '../_services/event.service';

@Component({
  selector: 'app-event-crud',
  templateUrl: './event-crud.component.html',
  styleUrls: ['./event-crud.component.css']
})
export class EventCrudComponent implements OnInit {
  crudForm: FormGroup;
  loading = false;
  events: ClimbingEvent[] = [];
  submitted = false;

  constructor(
    private formBuilder: FormBuilder,
    private eventService: EventService,
    private alertService: AlertService,
    private router: Router) {
  }

  private loadAllEvents() {
    this.eventService.findAll().pipe(first()).subscribe(events => {
      this.events = events;
    });
  }

  deleteEvent(id: number) {
    this.eventService.deleteById(id).pipe(first()).subscribe(() => {
      this.loadAllEvents();
    });
  }

  ngOnInit() {
    let event = new ClimbingEvent();
    if (localStorage.getItem('updatedEvent') != null) {
      event = JSON.parse(localStorage.getItem('updatedEvent'));
    }
    this.crudForm = this.formBuilder.group({
      id: [event.id, Validators.required],
      name: [event.name, Validators.required],
      date: [event.date, Validators.required],
      description: [event.description, Validators.required]
    });

    this.loadAllEvents();
  }

  // convenience getter for easy access to form fields
  get f() { return this.crudForm.controls; }

  update(id: number) {
    this.eventService.findById(id).pipe(first()).subscribe(
      event => {
        localStorage.setItem('updatedEvent', JSON.stringify(event));
        // this.router.navigateByUrl('/user-crud');

        let updatedEvent = new ClimbingEvent();
        if (localStorage.getItem('updatedEvent') != null) {
          updatedEvent = JSON.parse(localStorage.getItem('updatedEvent'));
        }

        this.crudForm.controls['name'].setValue(updatedEvent.name.toString());
        this.crudForm.controls['date'].setValue(updatedEvent.date.toString());
        this.crudForm.controls['description'].setValue(updatedEvent.description.toString());
      }
    );
  }

  onCreation() {
    this.submitted = true;

    if (this.crudForm.invalid) {
      return;
    }

    this.loading = true;
    this.eventService.save(this.crudForm.value)
      .pipe(first())
      .subscribe(
        data => {
          this.alertService.success('Creation successful', true);
          this.loading = false;
          this.loadAllEvents();
        },
        error => {
          this.alertService.error(error);
          this.loading = false;
        });
    this.submitted = false;
  }

  onUpdate() {
    this.submitted = true;

    if (this.crudForm.invalid) {
      return;
    }

    this.loading = true;

    this.eventService.update(this.crudForm.value)
      .pipe(first())
      .subscribe(
        () => {
          this.alertService.success('Update successful', true);
          this.loading = false;
          this.loadAllEvents();
        },
        error => {
          this.alertService.error(error);
          this.loading = false;
        });
    this.submitted = false;
  }

  get() {
    this.loading = true;

    this.update(this.crudForm.get('id').value);

    this.loading = false;
  }
}
