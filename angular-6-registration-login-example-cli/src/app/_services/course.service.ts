import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { environment } from '../../environments/environment';
import {ClimbingCourse} from '../_models';

@Injectable()
export class CourseService {
  uri = 'http://localhost:8080/';

  constructor(private http: HttpClient) { }

  findAll() {
    return this.http.get<ClimbingCourse[]>(this.uri + 'admin_list_courses');
  }

  findById(id: number) {
    return this.http.get(this.uri + 'admin_find_course_by_id?id=' + id);
  }

  save(aCourse: ClimbingCourse) {
    return this.http.post(this.uri + 'admin_add_course', aCourse);
  }

  update(aCourse: ClimbingCourse) {
    return this.http.post(this.uri + 'admin_update_course', aCourse);
  }

  deleteById(id: number) {
    return this.http.delete(this.uri + 'admin_delete_course?id=' + id);
  }

  // findByName(name: string) {
  //   return this.http.get(`${environment.apiUrl}/courses/` + name);
  // }

  addUserById(user_id: number, course_id: number) {
    return this.http.post(this.uri + 'user_enroll_to_course', {user_id: user_id, course_id: course_id});
  }

  removeUserById(user_id: number, course_id: number) {
    return this.http.post(this.uri + 'user_disenroll_from_course', {user_id: user_id, course_id: course_id});
  }
}
