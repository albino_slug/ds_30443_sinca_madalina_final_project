import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import {ClimbingEvent} from '../_models/climbing-event';

@Injectable()
export class EventService {
  uri = 'http://localhost:8080/';

  constructor(private http: HttpClient) { }

  findAll() {
    return this.http.get<ClimbingEvent[]>(this.uri + `/admin_list_events`);
  }

  findById(id: number) {
    return this.http.get(this.uri + `admin_find_event_by_id?id=` + id);
  }

  save(anEvent: ClimbingEvent) {
    return this.http.post(this.uri + 'admin_add_event', anEvent);
  }

  update(anEvent: ClimbingEvent) {
    return this.http.post(this.uri + 'admin_update_event', anEvent);
  }

  deleteById(id: number) {
    return this.http.delete(this.uri + 'admin_delete_event?id=' + id);
  }

  // findByName(name: string) {
  //   return this.http.get(`${environment.apiUrl}/events/` + name);
  // }

  addUserById(user_id: number, event_id: number) {
    return this.http.post(this.uri + 'user_attend_event', {user_id: user_id, event_id: event_id});
  }

  removeUserById(user_id: number, event_id: number){
    return this.http.post(this.uri + 'user_drop_event', {user_id: user_id, event_id: event_id});
  }
}
