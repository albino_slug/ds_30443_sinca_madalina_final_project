﻿import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import {ClimbingCourse, ClimbingEvent, User} from '../_models';
import {Observable} from 'rxjs';

@Injectable()
export class UserService {
  uri = 'http://localhost:8080/';

  constructor(private http: HttpClient) { }


  findAll() {
    return this.http.get<User[]>(this.uri + `/admin_list_users`);
  }

  findById(id: number) {
    return this.http.get(this.uri + 'admin_find_user_by_id?id=' + id);
  }

  register(user: User) {
    console.log(JSON.stringify(user));
    return this.http.post(this.uri + 'admin_add_user', user);
  }

  update(user: User) {
      return this.http.post(this.uri + 'admin_update_user', user);
  }

  deleteById(id: number) {
    return this.http.delete(this.uri + 'admin_delete_user?id=' + id);
  }
  //
  // findByEmail(email: string) {
  //   return this.http.get(`${environment.apiUrl}/users/` + email);
  // }
  //
  // findByRole(role: string) {
  //   return this.http.get(`${environment.apiUrl}/users/` + role);
  // }
  //
  // findByUsername(username: string) {
  //   return this.http.get(`${environment.apiUrl}/users/` + username);
  // }
  //
  // findByUsernameAndPassword(username: string, password: string) {
  //   return this.http.get(`${environment.apiUrl}/users/` + username + password);
  // }
  //
  findCoursesByUserId(id: number) {
    return this.http.get<ClimbingCourse[]>(this.uri + 'user_list_own_courses?id=' + id);
  }

  findEventsByUserId(id: number) {
    return this.http.get<ClimbingEvent[]>(this.uri + 'user_list_own_events?id=' + id);
  }

  generateGraphicStatistics(){
    return this.http.get(this.uri + 'user_age_stats');
  }
}
