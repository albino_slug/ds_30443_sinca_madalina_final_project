﻿import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import { map } from 'rxjs/operators';


@Injectable()
export class AuthenticationService {
    uri = 'http://localhost:8080/';

    constructor(private http: HttpClient) { }

  login(username: string, password: string) {
    console.log('loging in ' + username + password);
    const headers = new HttpHeaders({
      'Content-Type': 'application/json'
    });

    return this.http.post(this.uri + 'login', {username: username, password: password}, {headers: headers})
      .pipe(map(user => {
        console.log(JSON.stringify(user));
        // login successful if there's a jwt token in the response
        if (user != null) {
          // store user details and jwt token in local storage to keep user logged in between page refreshes
          localStorage.setItem('currentUser', JSON.stringify(user));
        }

        return user;
      }));
  }

    logout() {
        // remove user from local storage to log user out
        localStorage.removeItem('currentUser');
    }
}
