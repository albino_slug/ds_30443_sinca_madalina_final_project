import { Component, OnInit } from '@angular/core';
import {ClimbingEvent, User} from '../_models';
import {EventService, UserService} from '../_services';
import {first} from 'rxjs/operators';

@Component({
  selector: 'app-event-view',
  templateUrl: './event-view.component.html',
  styleUrls: ['./event-view.component.css']
})
export class EventViewComponent implements OnInit {
  events: ClimbingEvent[] = [];
  userEvents: ClimbingEvent[] = [];
  currentUser: User;

  constructor(private userService: UserService,
              private eventService: EventService) {
    this.currentUser = JSON.parse(localStorage.getItem('currentUser'));

    this.loadAllEvents();
    this.loadAllUserEvents();
  }

  ngOnInit() {
  }

  private loadAllEvents() {
    this.eventService.findAll().pipe(first()).subscribe(events => {
      this.events = events;
    });
  }

  private loadAllUserEvents() {
    this.userService.findEventsByUserId(this.currentUser.id).pipe(first()).subscribe(
      events => {
        this.userEvents = events;
      });
  }

  private attendEvent(id: number) {
    this.eventService.addUserById(this.currentUser.id, id).pipe(first()).subscribe(() => {
      this.loadAllUserEvents();
    });
  }

  private unattendEvent(id: number) {
    this.eventService.removeUserById(this.currentUser.id, id).pipe(first()).subscribe(() => {
      this.loadAllUserEvents();
    });
  }
}
