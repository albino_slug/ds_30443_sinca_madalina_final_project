﻿import {ClimbingEvent} from './climbing-event';
import {ClimbingCourse} from './climbing-course';

export class User {
  id: number;
  username: string;
  password: string;
  role: string;
  age: number;
  email: string;
  events: ClimbingEvent[];
  courses: ClimbingCourse[];
}
