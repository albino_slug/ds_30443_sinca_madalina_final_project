import {User} from './user';

export class ClimbingCourse {
    id: number;
    name: string;
    endDate: string;
    startDate: string;
    description: string;
    users: User[];
}
