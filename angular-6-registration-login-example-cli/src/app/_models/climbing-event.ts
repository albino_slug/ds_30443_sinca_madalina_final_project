import {User} from './user';

export class ClimbingEvent {
    id: string;
    name: string;
    date: string;
    description: string;
    users: User[];
}
