import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ClimbingCourse} from '../_models';
import {AlertService, CourseService} from '../_services';
import {Router} from '@angular/router';
import {first} from 'rxjs/operators';

@Component({
  selector: 'app-course-crud',
  templateUrl: './course-crud.component.html',
  styleUrls: ['./course-crud.component.css']
})
export class CourseCrudComponent implements OnInit {
  crudForm: FormGroup;
  loading = false;
  courses: ClimbingCourse[] = [];
  submitted = false;

  constructor(
    private formBuilder: FormBuilder,
    private courseService: CourseService,
    private alertService: AlertService,
    private router: Router) { }

  private loadAllCourses() {
    this.courseService.findAll().pipe(first()).subscribe(courses => {
      this.courses = courses;
    });
  }

  deleteCourse(id: number) {
    this.courseService.deleteById(id).pipe(first()).subscribe(() => {
      this.loadAllCourses();
    });
  }

  ngOnInit() {
    let course = new ClimbingCourse();
    if (localStorage.getItem('updatedCourse') != null) {
      course = JSON.parse(localStorage.getItem('updatedCourse'));
    }
    this.crudForm = this.formBuilder.group({
      id: [course.id, Validators.required],
      name: [course.name, Validators.required],
      startDate: [course.startDate, Validators.required],
      endDate: [course.endDate, Validators.required],
      description: [course.description, Validators.required]
    });

    this.loadAllCourses();
  }

  // convenience getter for easy access to form fields
  get f() { return this.crudForm.controls; }

  update(id: number) {
    this.courseService.findById(id).pipe(first()).subscribe(
      event => {
        localStorage.setItem('updatedCourse', JSON.stringify(event));
        // this.router.navigateByUrl('/user-crud');

        let updatedCourse = new ClimbingCourse();
        if (localStorage.getItem('updatedCourse') != null) {
          updatedCourse = JSON.parse(localStorage.getItem('updatedCourse'));
        }

        this.crudForm.controls['name'].setValue(updatedCourse.name.toString());
        this.crudForm.controls['startDate'].setValue(updatedCourse.startDate.toString());
        this.crudForm.controls['endDate'].setValue(updatedCourse.endDate.toString());
        this.crudForm.controls['description'].setValue(updatedCourse.description.toString());
      }
    );
  }

  onCreation() {
    this.submitted = true;

    if (this.crudForm.invalid) {
      return;
    }

    this.loading = true;
    this.courseService.save(this.crudForm.value)
      .pipe(first())
      .subscribe(
        data => {
          this.alertService.success('Creation successful', true);
          this.loading = false;
          this.loadAllCourses();
        },
        error => {
          this.alertService.error(error);
          this.loading = false;
        });
    this.submitted = false;
  }

  onUpdate() {
    this.submitted = true;

    if (this.crudForm.invalid) {
      return;
    }

    this.loading = true;

    this.courseService.update(this.crudForm.value)
      .pipe(first())
      .subscribe(
        () => {
          this.alertService.success('Update successful', true);
          this.loading = false;
          this.loadAllCourses();
        },
        error => {
          this.alertService.error(error);
          this.loading = false;
        });
    this.submitted = false;
  }

  get() {
    this.loading = true;

    this.update(this.crudForm.get('id').value);

    this.loading = false;
  }

}
