﻿import { Routes, RouterModule } from '@angular/router';

import { HomeComponent } from './home';
import { LoginComponent } from './login';
import { RegisterComponent } from './register';
import { UserCrudComponent } from './user-crud';
import { CourseCrudComponent } from './course-crud';
import { EventCrudComponent } from './event-crud';
import { UserHomeComponent} from './user-home';
import { AuthGuard } from './_guards';
import {EventViewComponent} from './event-view';
import { CourseViewComponent } from './course-view';

const appRoutes: Routes = [
    { path: '', component: HomeComponent, canActivate: [AuthGuard] },
    { path: 'login', component: LoginComponent },
    { path: 'register', component: RegisterComponent },
    { path: 'user-crud', component: UserCrudComponent },
    { path: 'course-crud', component: CourseCrudComponent },
    { path: 'event-crud', component: EventCrudComponent },
    { path: 'user-home', component: UserHomeComponent},
    { path: 'event-view', component: EventViewComponent},
    { path: 'course-view', component: CourseViewComponent },

    // otherwise redirect to home
    { path: '**', redirectTo: '' }
];

export const routing = RouterModule.forRoot(appRoutes);
