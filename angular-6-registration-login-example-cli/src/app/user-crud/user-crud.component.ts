import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {first} from 'rxjs/operators';

import { AlertService, UserService } from '../_services';
import {User} from '../_models';
import {Router} from '@angular/router';

@Component({
  selector: 'app-user-crud',
  templateUrl: './user-crud.component.html',
  styleUrls: ['./user-crud.component.css']
})
export class UserCrudComponent implements OnInit {
  crudForm: FormGroup;
  loading = false;
  currentUser: User;
  users: User[] = [];
  submitted = false;

  constructor(
    private formBuilder: FormBuilder,
    private userService: UserService,
    private alertService: AlertService,
    private router: Router) {
    this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
  }

  private loadAllUsers() {
    this.userService.findAll().pipe(first()).subscribe(users => {
      this.users = users;
    });
  }

  deleteUser(id: number) {
      this.userService.deleteById(id).pipe(first()).subscribe(() => {
          this.loadAllUsers();
      });
  }

  ngOnInit() {
    let user = new User();
    if (localStorage.getItem('updatedUser') != null) {
      user = JSON.parse(localStorage.getItem('updatedUser'));
    }
    this.crudForm = this.formBuilder.group({
      id: [user.id, Validators.required],
      email: [user.email, Validators.required],
      role: [user.role, Validators.required],
      username: [user.username, Validators.required],
      password: [user.password, [Validators.required, Validators.minLength(6)]],
      age: [user.age, Validators.required]
    });

    this.loadAllUsers();
  }

  // convenience getter for easy access to form fields
  get f() { return this.crudForm.controls; }

  update(id: number) {
    this.userService.findById(id).pipe(first()).subscribe(
      user => {
         localStorage.setItem('updatedUser', JSON.stringify(user));
        // this.router.navigateByUrl('/user-crud');

        let updatedUser = new User();
        if (localStorage.getItem('updatedUser') != null) {
          updatedUser = JSON.parse(localStorage.getItem('updatedUser'));
        }

        this.crudForm.controls['role'].setValue(updatedUser.role.toString());
        this.crudForm.controls['email'].setValue(updatedUser.email.toString());
        this.crudForm.controls['username'].setValue(updatedUser.username.toString());
        this.crudForm.controls['password'].setValue(updatedUser.password.toString());
        this.crudForm.controls['age'].setValue(updatedUser.age.toString());
      }
    );
  }

  onCreation() {
    this.submitted = true;

    if (this.crudForm.invalid) {
      return;
    }

    this.loading = true;
    this.userService.register(this.crudForm.value)
      .pipe(first())
      .subscribe(
        data => {
          this.alertService.success('Registration successful', true);
          this.loading = false;
          this.loadAllUsers();
        },
        error => {
          this.alertService.error(error);
          this.loading = false;
        });
    this.submitted = false;
  }

  onUpdate() {
    this.submitted = true;

    if (this.crudForm.invalid) {
      return;
    }

    this.loading = true;

    this.userService.update(this.crudForm.value)
      .pipe(first())
      .subscribe(
        () => {
          this.alertService.success('Update successful', true);
          this.loading = false;
          this.loadAllUsers();
        },
        error => {
          this.alertService.error(error);
          this.loading = false;
        });
    this.submitted = false;
  }

  get() {
    this.loading = true;

    this.update(this.crudForm.get('id').value);

    this.loading = false;
  }
}
