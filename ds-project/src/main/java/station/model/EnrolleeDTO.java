package station.model;

import lombok.*;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@Getter
@Setter
public class EnrolleeDTO {
    private Integer user_id;
    private Integer course_id;
}
