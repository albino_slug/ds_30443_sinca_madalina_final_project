package station.service;

import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.Map;

@Service
public interface StatsService {

    Boolean createUserAgePieChart();
    Map<String, String> getImage() throws IOException;
    String getDataURI();

}
