package station.service;

import station.model.User;
import station.repository.UserRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class AuthenticationServiceImpl implements AuthenticationService{
    @Autowired
    private UserRepo userRepo;

    @Override
    public Optional<User> loadByNameAndPassword(String username, String password) {
        Optional<User> resultUser = userRepo.findByUsernameAndPassword(username, password);
        if (resultUser.isPresent()){
            return resultUser;
        }
        else{
            return Optional.empty();
        }
    }

}
