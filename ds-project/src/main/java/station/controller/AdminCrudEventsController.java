package station.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import station.model.Event;
import station.service.EventService;

import java.util.List;
import java.util.Optional;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
public class AdminCrudEventsController {
    @Autowired
    private EventService eventService;

    @RequestMapping(value = "/admin_add_event", method = RequestMethod.POST)
    public ResponseEntity addNewEvent(@RequestBody Event event) {
        Event savedEvent = eventService.save(event);
        if (savedEvent != null){
            return new ResponseEntity<>(HttpStatus.CREATED);
        }
        return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }

    @RequestMapping(value = "/admin_delete_event", method = RequestMethod.DELETE)
    public ResponseEntity deleteEvent(@RequestParam("id") Integer id){
        if (eventService.deleteById(id)){
            return new ResponseEntity<>(HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }

    @RequestMapping(value = "/admin_list_events", method = RequestMethod.GET)
    public List<Event> listAllEvents(){
        return eventService.findAll();
    }

    @RequestMapping(value = "/admin_update_event", method = RequestMethod.POST)
    public ResponseEntity updateEvent(@RequestBody Event event) {
        Optional foundEvent = eventService.findById(event.getId());
        if (foundEvent.isPresent()){
            Event savedEvent = eventService.save(event);
            if (savedEvent != null){
                return new ResponseEntity<>(HttpStatus.OK);
            }
        }
        return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }

    @RequestMapping(value = "/admin_find_event_by_id", params = {"id"}, method = RequestMethod.GET)
    public Event findEvent(@RequestParam(value = "id") int id) {
        Optional foundEvent = eventService.findById(id);
        if (foundEvent.isPresent()){
            return (Event)foundEvent.get();
        }
        return new Event();
    }
}
