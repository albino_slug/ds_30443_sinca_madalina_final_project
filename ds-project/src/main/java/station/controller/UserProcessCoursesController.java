package station.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import station.model.Course;
import station.model.EnrolleeDTO;
import station.model.Mail;
import station.model.User;
import station.service.CourseService;
import station.service.MailService;
import station.service.UserService;

import javax.servlet.http.HttpSession;
import java.util.List;
import java.util.Optional;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
public class UserProcessCoursesController {
    @Autowired
    private CourseService courseService;

    @Autowired
    private UserService userService;

    @Autowired
    private MailService mailService;

    @RequestMapping(value = "/user_list_all_courses", params = {"id"}, method = RequestMethod.GET)
    public List<Course> listAllCourses(){
        return courseService.findAll();
    }

    @RequestMapping(value = "/user_list_own_courses", method = RequestMethod.GET)
    public List<Course> listAllOwnCourses(@RequestParam("id") int id){
        //return userService.findCoursesByUserId(Integer.parseInt(httpSession.getAttribute("userId").toString()));

        return userService.findCoursesByUserId(id);
    }

    @RequestMapping(value = "/user_enroll_to_course", method = RequestMethod.POST)
    public ResponseEntity enrollToCourse(@RequestBody EnrolleeDTO enrolleeDTO){
        // User user = userService.findById(Integer.parseInt(httpSession.getAttribute("userId").toString())).get();

        Optional optionalUser = userService.findById(enrolleeDTO.getUser_id());
        if (optionalUser.isPresent()){
            User user = (User)optionalUser.get();
            Boolean addedUser = courseService.addUserById(enrolleeDTO.getCourse_id(), user.getId());
            Boolean addedCourse = userService.addCourseById(user.getId(), enrolleeDTO.getCourse_id());
            if (addedUser && addedCourse){
                mailService.sendSimpleMessage(new Mail("awa.climbing.station@gmail.com", user.getEmail(), "Enrollment Confirmation", "You have successfully enrolled in course <<" + courseService.findById(enrolleeDTO.getCourse_id()).get().getName() + ">>."));
                return new ResponseEntity<>(HttpStatus.OK);
            }
        }
        return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }

    @RequestMapping(value = "/user_disenroll_from_course", method = RequestMethod.POST)
    public ResponseEntity dropCourse(@RequestBody EnrolleeDTO enrolleeDTO){
        //User user = userService.findById(Integer.parseInt(httpSession.getAttribute("userId").toString())).get();

        Optional optionalUser = userService.findById(enrolleeDTO.getUser_id());
        if (optionalUser.isPresent()) {
            User user = (User) optionalUser.get();
            Boolean removedUser = courseService.removeUserById(enrolleeDTO.getCourse_id(), user.getId());
            Boolean removedCourse = userService.removeCourseById(user.getId(), enrolleeDTO.getCourse_id());
            if (removedUser && removedCourse){
                mailService.sendSimpleMessage(new Mail("awa.climbing.station@gmail.com", user.getEmail(), "Dropping Out Confirmation", "You have successfully dropped out of the course <<" + courseService.findById(enrolleeDTO.getCourse_id()).get().getName() + ">>."));
                return new ResponseEntity<>(HttpStatus.OK);
            }
        }
        return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }
}
