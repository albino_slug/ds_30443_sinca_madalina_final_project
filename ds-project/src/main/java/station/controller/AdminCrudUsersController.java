package station.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import station.model.User;
import station.service.UserService;

import java.util.List;
import java.util.Optional;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
public class AdminCrudUsersController {
    @Autowired
    private UserService userService;

    @RequestMapping(value = "/admin_add_user", method = RequestMethod.POST)
    public ResponseEntity addNewUser(@RequestBody User user) {
        System.out.println(user.toString());
        User savedUser = userService.save(user);
        if (savedUser != null){
            return new ResponseEntity<>(HttpStatus.CREATED);
        }
        return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }

    @RequestMapping(value = "/admin_delete_user", method = RequestMethod.DELETE)
    public ResponseEntity deleteUser(@RequestParam(value = "id") int id){
        if (userService.deleteById(id)){
            return new ResponseEntity<>(HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }

    @RequestMapping(value = "/admin_list_users", method = RequestMethod.GET)
    public List<User> listAllUsers(){
        return userService.findAll();
    }

    @RequestMapping(value = "/admin_update_user", method = RequestMethod.POST)
    public ResponseEntity updateUser(@RequestBody User user) {
        Optional foundUser = userService.findById(user.getId());
        if (foundUser.isPresent()){
            User savedUser = userService.save(user);
            if (savedUser != null){
                return new ResponseEntity<>(HttpStatus.OK);
            }
        }
        return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }

    @RequestMapping(value = "/admin_find_user_by_id", params = {"id"}, method = RequestMethod.GET)
    public User findEvent(@RequestParam(value = "id") int id) {
        Optional foundUser = userService.findById(id);
        if (foundUser.isPresent()){
            return (User)foundUser.get();
        }
        return new User();
    }
}
