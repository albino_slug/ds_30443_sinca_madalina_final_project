package station.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import station.model.Role;
import station.model.User;
import station.service.AuthenticationService;

import java.util.HashMap;
import java.util.Optional;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
public class LoginController {

    @Autowired
    AuthenticationService authenticationService;

    @RequestMapping(value = "/login", method = RequestMethod.POST)
//    public ResponseEntity login(@RequestParam("username") String username, @RequestParam("password") String password) {
    public ResponseEntity<User> login(@RequestBody User user) {
//    public ResponseEntity<User> login(@RequestBody String user) {
        System.out.println(user);
        Optional<User> optionalUser = authenticationService.loadByNameAndPassword(user.getUsername(), user.getPassword());
        if (optionalUser.isPresent()) {
            return new ResponseEntity<>(optionalUser.get(), HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }
}